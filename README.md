# pFront

pFront is a green JavaScript framework that create a parallel front in webworker before send it to the DOM. It is based on a component architecture.

## Green framework
A green framework is a small framework with a minimum of automatization, in order to avoid useless actions. The goal is to use less memory as possible. That why the library has to be small and frugal 

## pFront Principle
With this library you can create and manage easily page directly through javascript. Pages are constituted by component. Pages and components are invoked in webworker in order to improve performance.
Each component generate it's own view, creating a kind of virual DOM. Component are automatically updated when its state change (like in react).
You can use this library for a project from scratch or on a server rendered page.

## License
![CRAN/METACRAN](https://img.shields.io/cran/l/pack?logo=GPL-3)(https://choosealicense.com/licenses/gpl-3.0/)

[ci]: https://about.gitlab.com/gitlab-ci/
[index.html]: https://gitlab.com/pages/plain-html/blob/master/public/index.html
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
